# Oneplus 3T Ubuntu Touch Halium9 GSI install guide

## Downloads
- download oneplus3 twrp treble (twrp-op3treble-3.3.1-1.img)
  - https://mega.nz/folder/UgdQRYSD#8s-_u2HJQZDEqNnFOnejxQ/folder/Ypd2BKhS

- download [A/B] LineageOS 16.0 Treble system-as-root (lineage-16.0-20190826-UNOFFICIAL-oneplus3.zip)
  - https://mega.nz/folder/UgdQRYSD#8s-_u2HJQZDEqNnFOnejxQ

- download erfan ubports gsi and halium ramdisk
  - https://mirrors.lolinet.com/firmware/halium/GSI/ubports_GSI_installer_v10.zip
  - https://mirrors.lolinet.com/firmware/halium/GSI/tools/halium-ramdisk.zip

- download halium-boot.img
  - https://drive.google.com/drive/folders/1vnJEKkhO3xqH-fWWG55-yxwx5K1EeKq7

## Install
### Flash Treble TWRP

```
fastboot flash recovery twrp-op3treble-3.3.1-1.img
```

### Boot to TWRP and create vendor partition

- advanced > terminal > treblize

### Flash LineageOS Treble system-as-root
```
adb sideload lineage-16.0-20190826-UNOFFICIAL-oneplus3.zip
```
- reboot & check that lineage boots

### Install Erfan UBports GSI

- boot to fastboot and flash halium-boot
```
fastboot flash boot halium-boot.img
```
- boot to recovery, flash ubports gsi and halium-ramdisk.zip
```
adb sideload ubports_GSI_installer_v10.zip
adb sideload halium-ramdisk.zip
```
- reboot to ubuntu touch

- enable ssh

- ssh to phablet@ubuntu-phablet

- update ubports
```
sudo apt install tmux
tmux
sudo apt-mark hold hfd-service hfd-service-tools libqt5feedback5-hfd lxc-android-config powerd qml-module-hfd  repowerd repowerd-data repowerd-tools
sudo apt update
sudo apt upgrade
```

## Install agnos in chroot
- set up ubuntu 20.04 chroot
```
sudo apt install debootstrap schroot
sudo mkdir -p /home/chroot/focal_arm64
sudo debootstrap --variant=minbase --arch=arm64 focal /home/phablet/chroot/focal_arm64 http://ports.ubuntu.com/ubuntu-ports
schroot -c focal_arm64 -u root
```
- build agnos system on ubuntu machine, copy build/filesystem.tar over to ubuntu-touch
```
cd agnos-builder
./build_system.sh
bzip2 build/filesystem.tar
scp build/filesystem.tar.bz2 phablet@ubuntu-phablet:
schroot -c focal_arm64 -u root
cd /
tar xvjf /home/phablet/filesystem.tar.bz2
```

## Conclusion

openpilot compiles but does not start (missing wayland). ubuntu touch is using old version of mir and lomiri/unity8 which does not support wayland. next steps will have to wait for new major version of ubuntu touch (ubuntu 20.04 + wayland support in mir). openpilot sensors, opencl and camera support would have to be ported to support hybris

## References
- https://forums.ubports.com/topic/4481/halium9-gsi-oneplus-3-t
- https://github.com/OP3-Halium/Documentation

## Misc ideas and notes

- replace ubports rootfs with agnos
  - build minimal working ubports rootfs (ssh enabled, no gui)
    https://docs.ubports.com/en/latest/porting/build_and_boot/Halium_install.html
      - latest rootfs from ci
      https://ci.ubports.com/job/xenial-hybris-android9-rootfs-arm64/lastSuccessfulBuild/artifact/ubuntu-touch-android9-arm64.tar.gz
      - latest lxc android rootfs
      https://ci.ubports.com/job/UBportsCommunityPortsJenkinsCI/job/ubports%252Fcommunity-ports%252Fjenkins-ci%252Fgeneric_arm64/job/main/lastSuccessfulBuild/artifact/halium_halium_arm64.tar.xz

- twrp treblize script
```
/sbin/sgdisk --typecode=5:8300 /dev/block/sdf
/sbin/sgdisk --change-name=5:vendor /dev/block/sdf
```

- resize any gsi image to 3.1G for oneplus3 (not needed with system-on-root)
```
sudo simg2img <system.img> system.ext4.img
sudo mount -t ext4 -o loop system.ext4.img system/
sudo chown -R <username>:<username> system/
#then enter the system/app folder and remove gmail, maps, google play music, drive, etc.
makeext4fs -T 0 filecontexts -l 3154116608 -L system -system -s flash.img system/
```
